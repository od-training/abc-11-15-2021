import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, share, switchMap } from 'rxjs/operators';

import { VideoDataService } from '../../video-data.service';
import { Video } from '../types';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss'],
})
export class VideoDashboardComponent {
  videoList: Observable<Video[]>;
  currentVideo: Observable<Video | undefined>;

  constructor(
    route: ActivatedRoute,
    private vds: VideoDataService,
    private router: Router
  ) {
    this.videoList = vds.loadVideos().pipe(
      map((videos) =>
        videos.map((video) => ({
          ...video,
          title: video.title.toUpperCase(),
        }))
      )
    );

    this.currentVideo = route.queryParamMap.pipe(
      map((paramMap) => paramMap.get('videoId')),
      switchMap((id) => this.vds.loadVideo(id ?? '')),
      share()
    );
  }

  handleVideoSelected(video: Video) {
    this.router.navigate([], {
      queryParams: {
        videoId: video.id,
      },
      queryParamsHandling: 'merge',
    });
  }
}
