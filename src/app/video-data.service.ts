import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Video } from './dashboard/types';
import { Observable } from 'rxjs';

const apiUrl = 'https://api.angularbootcamp.com';

@Injectable({
  providedIn: 'root',
})
export class VideoDataService {
  constructor(private http: HttpClient) {}

  loadVideos(): Observable<Video[]> {
    return this.http.get<Video[]>(apiUrl + '/videos');
  }

  loadVideo(id: string) {
    return this.http.get<Video>(apiUrl + '/videos/' + id);
  }
}
